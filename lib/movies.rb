require_relative '../db/connection'
require_relative 'movies/movie'
require_relative 'movies/rating'

class MovHtml
  def translate
    data = File.read("/home/abe/Hack/movies/movies.html")
    18.times do
      x = 1
      z = Movie.find(x)
      r = Rating.find(x)
      data = data.sub("@title", "#{z.title}")
      data = data.sub("@genre", "#{z.genre}")
      data = data.sub("@director", "#{z.director}")
      data = data.sub("@actors", "#{z.actors}")
      data = data.sub("@rate", "#{r.imd}")
      data = data.sub("@released", "#{z.released_date}")
      data = data.sub("@awards", "#{z.awards}")
      data = data.sub("@post", "#{z.poster}")
    end
    fileHtml = File.new("movie_page.html", "w+")
    fileHtml.puts data
    fileHtml.close()
  end
end
MovHtml.new.translate



