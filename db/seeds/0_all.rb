require 'rest-client'
require 'json'

class Fetcher
  attr_accessor :apikey, :mov, :list
  def initialize
    @apikey = "877f60e7"
    @list=[]
  end
  def movie_ask
    @list=["John Wick", "John Wick: chapter 2", "Guardians of the Galaxy", "Guardians of the Galaxy vol. 2", "Marley & Me", "The last Samurai", "Good Will Hunting", "Aladdin", "Patch Adams", "inception", "interstellar", "Pacific Rim", "Iron Man", "Iron Man 2", "The Amazing Spider-man", "The Amazing Spider-man 2", "Harry Potter and the Deathly Hallows", "Cars"]
    for i in 0...@list.length do
      @list[i]=@list[i].downcase
      @list[i]=@list[i].gsub(" ", "+")
      @list[i]=@list[i].gsub(":", "%3A")
      @list[i]=@list[i].gsub("&", "%26")
    end
  end
  def matcher
    movie_ask
    @list.each_with_index do |x, num=0| { x => num+=1 }
      url= "http://www.omdbapi.com/?t=#{x}&apikey=#{@apikey}"
      @mov = JSON.parse(RestClient.get(url), :symbolize_names => true )
      if num<7 then
        user='Juan'
      elsif num>6 and num<13 then
        user='Ibrahim'
      else
        user='Andres'
      end
    Rating.create(imd: @mov[:Ratings][0][:Value], rotten_tomatoes: @mov[:Ratings][1][:Value], metacritic: @mov[:Ratings][2][:Value])
    param1=@mov[:Ratings][0][:Value]
    param2=@mov[:Ratings][1][:Value]
    param3=@mov[:Ratings][2][:Value]
    Movie.create(title: @mov[:Title], released_date: @mov[:Released], genre: @mov[:Genre], director: @mov[:Director], actors: @mov[:Actors], awards: @mov[:Awards], poster: @mov[:Poster], rating_id: Rating.where(imd: param1, rotten_tomatoes: param2, metacritic: param3).ids[0], production: @mov[:Production], favourite_by: user)
    end
  end
end
Fetcher.new.matcher
