class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :released_date
      t.text :genre
      t.string :director
      t.text :actors
      t.text :awards
      t.text :poster
      t.references :rating, foreign_key: true
      t.string :production
      t.string :favourite_by
    end
  end
end
