class CreateRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :ratings do |t|
      t.string :imd
      t.string :rotten_tomatoes
      t.string :metacritic
    end
  end
end
