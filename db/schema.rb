# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_30_173839) do

  create_table "movies", force: :cascade do |t|
    t.string "title"
    t.string "released_date"
    t.text "genre"
    t.string "director"
    t.text "actors"
    t.text "awards"
    t.text "poster"
    t.integer "rating_id"
    t.string "production"
    t.string "favourite_by"
    t.index ["rating_id"], name: "index_movies_on_rating_id"
  end

  create_table "ratings", force: :cascade do |t|
    t.string "imd"
    t.string "rotten_tomatoes"
    t.string "metacritic"
  end

end
