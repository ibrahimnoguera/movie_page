Data for ports and countries got from:
https://geonode.wfp.org/geoserver/wfs?srsName=EPSG%3A4326&typename=geonode%3Awld_trs_ports_wfp&outputFormat=json&version=1.0.0&service=WFS&request=GetFeature

Data for container acronyms got from:
https://en.wikipedia.org/wiki/Bill_of_lading

House Bill of Lading example gotten from:
https://www.letterofcredit.biz/images/housebilloflading.jpg
http://www.datamyne.com/multisite/wp-content/blogs.dir/1/files/2013/02/Ethanol_bill_of_lading_detail-740x1024.jpeg
https://www.starresoft.com/images/bolform1.png

NVOCC list from:
https://www2.fmc.gov/oti/NVOCC.aspx

List of the biggest container ships:
https://en.wikipedia.org/wiki/List_of_largest_container_ships

List of all shipping companies:
https://en.wikipedia.org/wiki/List_of_freight_ship_companies

